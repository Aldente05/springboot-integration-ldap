package spring.boot.ldap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by f.putra on 01/03/18.
 */
@SpringBootApplication
public class LDAPApplication implements CommandLineRunner {


    @Override
    public void run(String... args) throws Exception {
        SpringApplication.run(LDAPApplication.class, args);
    }
}
